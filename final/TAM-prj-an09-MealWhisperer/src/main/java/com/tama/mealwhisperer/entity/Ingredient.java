package com.tama.mealwhisperer.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by norbert.durcansky on 10/8/2017.
 */

@Entity
public class Ingredient {

    @PrimaryKey(autoGenerate = true)
    private Integer uid;

    @ColumnInfo(name ="name")
    private String name;

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Ingredient setUidO(Integer uid) {
        this.uid = uid;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Ingredient setNameO(String name) {
        this.name = name;
        return this;
    }

    @Override
    public boolean equals(Object obj) {

        if((obj == null) || (obj.getClass() != this.getClass())) { return false; }

        return uid.equals(((Ingredient) obj).getUid());
    }
}
