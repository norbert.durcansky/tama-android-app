package com.tama.mealwhisperer.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by norbert.durcansky on 10/8/2017.
 */

@Entity
public class User {

    @PrimaryKey(autoGenerate = true)
    private Integer uid;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "age")
    private String age;

    @ColumnInfo(name = "picture_path")
    private String picturePath;

    public Integer getUid() {
        return uid;
    }

    public User setUidO(Integer uid) {
        this.uid = uid;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setNameO(String name) {
        this.name = name;
        return this;
    }

    public String getAge() {
        return age;
    }

    public User setAgeO(String age) {
        this.age = age;
        return this;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public User setPicturePathO(String picturePath) {
        this.picturePath = picturePath;
        return this;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }
}