package com.tama.mealwhisperer;

import android.app.Application;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by norbert.durcansky on 10/8/2017.
 */

public class MealWhisperer extends Application {
    public static MealWhisperer INSTANCE;

    AppDatabase appDatabase;

    public static MealWhisperer get() {
        return INSTANCE;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "mealWhisperer").allowMainThreadQueries().addCallback(rdc).build();

        INSTANCE = this;
    }

    /** Here insert all initial data */
    RoomDatabase.Callback rdc = new RoomDatabase.Callback(){
        public void onCreate (SupportSQLiteDatabase db){
            db.execSQL("INSERT INTO ingredient (uid,name) values (-1,'Milk')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-2,'Cocoa')");

            db.execSQL("INSERT INTO ingredient (uid,name) values (-3,'Potatoes')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-4,'Eggs')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-5,'Tomatoes')");

            db.execSQL("INSERT INTO ingredient (uid,name) values (-6,'Coconut oil')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-7,'Vanilla extract')");

            db.execSQL("INSERT INTO ingredient (uid,name) values (-8,'Chicken breasts')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-9,'Taco seasoning mix')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-10,'Salsa verde')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-11,'Cilantro')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-12,'Tortilla chips')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-13,'Cheese')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-14,'Sour cream')");

            db.execSQL("INSERT INTO ingredient (uid,name) values (-15,'Chocolate')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-16,'Butter')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-17,'Brown sugar')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-18,'Almonds')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-19,'Brown rice flour')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-20,'Nuts')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-21,'Apple')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-22,'Snickers candy bar')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-23,'Pecans')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-24,'Whipped Cream')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-25,'Sweet potato')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-26,'White sugar')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-27,'Butter milk')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-29,'Cinnamon')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-30,'NutMeg')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-31,'Frozen strawberries')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-32,'Frozen blueberries')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-33,'Oatmeal')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-34,'Protein powder')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-35,'Banana')");

            db.execSQL("INSERT INTO ingredient (uid,name) values (-36,'Salt')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-37,'Soda')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-38,'Flour')");

            db.execSQL("INSERT INTO ingredient (uid,name) values (-39,'Ground beef')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-40,'Green bell pepper')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-41,'Large onion')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-42,'Original Bisquick® mix')");

            db.execSQL("INSERT INTO ingredient (uid,name) values (-43,'Pork tenderloin')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-44,'Carrot')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-45,'Chestnuts')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-46,'Soy sauce')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-47,'Sesame oil')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-48,'Ground ginger')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-49,'Rice')");




            db.execSQL("INSERT INTO ingredient (uid,name) values (-50,'Pheasant')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-51,'Diced tomatoes')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-52,'White zinfandel')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-53,'Linguine')");

            db.execSQL("INSERT INTO ingredient (uid,name) values (-54,'Cider')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-55,'Orange juice')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-56,'Lemon juice')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-57,'Cloves')");

            db.execSQL("INSERT INTO ingredient (uid,name) values (-58,'Garlic powder')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-59,'Tortilla')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-60,'Lime')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-61,'Cabbage')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-62,'Panko bread crumbs')");

            db.execSQL("INSERT INTO ingredient (uid,name) values (-63,'Graham crackers')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-64,'Vanilla ice cream')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-65,'Oreos')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-66,'Brown m&ms')");

            db.execSQL("INSERT INTO ingredient (uid,name) values (-67,'Olive oil')");
            db.execSQL("INSERT INTO ingredient (uid,name) values (-68,'Butternut squash')");

            /** Grandma’s Cider **/

            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-11,-54,'1 gallon')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-11,-55,'1/2 cup ')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-11,-56,'1/4 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-11,-26,'2 tbsp')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-11,-29,'3 sticks')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-11,-57,'1-1/2 tsp')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-11,-30,'1 tsp')");
            db.execSQL("INSERT INTO recipe (uid,name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite)" +
                    " VALUES (-11,'Grandma Cider', 'grandma_cider', '15 minutes', 2, 'drink', " +
                    "'Mix all ingredients in a large pan. Bring just to a boil. Strain.', 0, 0)");

            /** Pheasant Phenom */
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-10,-50,'1')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-10,-51,'2 cans')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-10,-52,'2 cups')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-10,-53,'1 linguine')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-10,-13,'1 cup')");
            db.execSQL("INSERT INTO recipe (uid,name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite)" +
                    " VALUES (-10,'Pheasant Phenom', 'pheasant_phenom', '30 minutes', 4, 'main', " +
                    "'Defrost pheasant, if frozen. Place pheasant meat in pot of boiling water. Boil meat for 10 to 15 minutes (cooking times vary depending on the size of the pheasant). Remove pot from heat. Remove meat from bone. Shred and/or dice meat." +
                    "\nIn large saucepan, combine cooked, shredded pheasant, diced tomatoes, pasta sauce mix and white zinfandel. Stir ingredients and bring to a boil. Reduce heat and simmer for 10 minutes.\n" +
                    "Prepare linguine according to package directions. Place linguine on plate, top with sauce, add shredded cheese.', 0, 0)");



            /** Pork ‘N’ Veggie Packets */
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-9,-43,'1 pound')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-9,-44,'2 cups')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-9,-45,'1')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-9,-40,'1 medium')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-9,-41,'2 green')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-9,-46,'1/4 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-9,-47,'4 tsp')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-9,-48,'1 tsp')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-9,-49,'100g')");
            db.execSQL("INSERT INTO recipe (uid,name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite)" +
                    " VALUES (-9,'Pork Packets', 'pork_packets', '40 minutes', 4, 'main', " +
                    "'Divide pork, broccoli, carrots, water chestnuts, green pepper and onions evenly among four pieces of double-layered heavy-duty foil, but do not fold close. Combine the soy sauce, sesame oil and ginger; drizzle over pork and vegetables. Fold foil around filling and seal tightly." +
                    "\nGrill, covered, over medium heat for 8-10 minutes or until vegetables are tender and pork is no longer pink. Serve with rice if desired.', 0, 0)");





            /** Cheeseburger Pie */
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-8,-39,'1 lb')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-8,-40,'1/4')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-8,-36,'1/2 tsp')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-8,-13,'1 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-8,-42,'1/2 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-8,-41,'1')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-8,-1,'1 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-8,-4,'2')");
            db.execSQL("INSERT INTO recipe (uid,name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite)" +
                    " VALUES (-8,'Cheeseburger Pie', 'cheeseburger_pie', '35 minutes', 4, 'main', " +
                    "'Heat oven to 400°F. Spray 9-inch glass pie plate with cooking spray.\n" +
                    "In 10-inch skillet, cook beef and onion over medium heat 8 to 10 minutes, stirring occasionally, until beef is brown; drain. Stir in salt. Spread in pie plate. Sprinkle with cheese.\n" +
                    "In small bowl, stir remaining ingredients with fork or wire whisk until blended. Pour into pie plate.\n" +
                    "Bake about 25 minutes or until knife inserted in center comes out clean.', 0, 0)");

            /** Ma donuts  */
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-7,-26,'2 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-7,-30,'100 g')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-7,-36,'10 g')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-7,-16,'4 tbsp')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-7,-4,'4')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-7,-37,'2 level tsp')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-7,-27,'2 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-7,-38,'3-1/2 cup')");
            db.execSQL("INSERT INTO recipe (uid,name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite)" +
                    " VALUES (-7,'Ma Donuts', 'ma_donuts', '40 minutes', 4, 'main', " +
                    "'Preheat deep fryer to 375 degrees F\nMix together soda and buttermilk. Cover the top of the sugar with nutmeg. Mix together\n" +
                    "Add the rest of the ingredients and roll out dough 1/2 inch thick, cut with floured donut cutter or make donut balls. Deep fry until brown, flip once.', 0, 0)");












            /** recipe -> ingredients  Hot chocolate*/
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-1,-1,'500ml')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-1,-6,'5ml')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-1,-7,'10g')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-1,-2,'25g')");
            db.execSQL("INSERT INTO recipe (uid,name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite) VALUES (-1,'Hot Chocolate', 'hot_chocolate', '2 minutes', 1, 'drink', 'Combine all ingredients in blender and blend to combine.\nServe warm and enjoy!', 1, 1)");

            /**Cheesy Chicken Chilaquiles */
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-2,-8,'500g')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-2,-9,'10g')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-2,-10,'4 cups')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-2,-11,'3/4 cups')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-2,-12,'12 cups')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-2,-13,'2 cups')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-2,-14,'1 cups')");
            db.execSQL("INSERT INTO recipe (uid,name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite) " +
                    "VALUES (-2,'Cheesy Chicken', 'cheesy_chicken_chilauiles', '30 minutes', 4, 'main'," +
                    " 'Cut chicken into 1-in pieces. Combine chicken and seasoning mix in skillet and cook; mix well. Spread chicken evenly over bottom of baker. Microwave, covered, on HIGH 4 minutes; stir to separate chicken. Microwave, covered, an additional 4 minutes or until chicken is no longer pink. Drain..\n" +
                    "Combine salsa and broth. Chop cilantro. Arrange half of the tortilla chips over bottom of same baker, breaking chips; top with half each of the chicken, salsa mixture and cheeses. Sprinkle with ½ cup of the cilantro. Repeat layers one time, ending with cheeses.\n" +
                    "Microwave, covered on high 5-7 minutes or until cheeses are melted and most of the liquid is absorbed. Let stand, covered, 5 minutes. Sprinkle with remaining ¼ cup cilantro. Serve with sour cream, if desired.', 0, 0)");

            /**Classic chewy brownie */
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-3,-15,'500g')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-3,-16,'250g')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-3,-17,'1 cup')");

            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-3,-4,'2')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-3,-7,'few drops')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-3,-18,'1/2 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-3,-19,'350g')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-3,-20,'1 cup')");

            db.execSQL("INSERT INTO recipe (uid,name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite) VALUES (" +
                    "-3,'Chocolate Brownies', 'classic_chewy_brownie', '40 minutes', 6, 'main', " +
                    "'Preheat the oven to 350 degrees F. Grease and line an 11×7 inch baking pan. Place the chocolate and butter in a large heatproof bowl over a pan of simmering water and melt. Stir in all the remaining ingredients and combine well." +
                    "\nPour the mixture into the prepared pan and place in the preheated oven for 30 minutes, until slightly springy in the center. Remove from the oven and cool for 10 minutes in the pan, then mark to divide into 15 squares." +
                    "', 0, 0)");

            /** Snicker Salad   */
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-4,-21,'6 chopped')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-4,-22,'6 choped')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-4,-23,'1/2 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-4,-24,'125ml')");
            db.execSQL("INSERT INTO recipe (uid,name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite)" +
                    " VALUES (-4,'Snicker Salad', 'snicker_salad', '10 minutes', 1, 'salad', " +
                    "'Combine apples, snickers and pecans; mix well.\nFold in whipped topping and refrigerate.\nBest served the same day made.', 0, 0)");

            /** Sweet Potato Casserole */
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-5,-25,'2 large')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-5,-26,'1 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-5,-17,'1/2 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-5,-16,'1 stick')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-5,-4,'4')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-5,-27,'1/2 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-5,-1,'1/2 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-5,-23,'1/2 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-5,-7,'20g')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-5,-29,'10g')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-5,-30,'10g')");
            db.execSQL("INSERT INTO recipe (uid,name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite) VALUES " +
                    "(-5,'Potato Casserole', 'sweet_potato_casserole', '90 minutes', 2, 'main', " +
                    "'Preheat the oven to 325 degrees F. Spray a 13×9-inch baking dish with nonstick cooking spray.\n" +
                    "Peel sweet potatoes and grate coarsely to yield 6 cups.\n" +
                    "Combine granulated sugar and butter in a large bowl. Beat with an electric mixer set at high speed until creamy, about 2 minutes. Add eggs one at a time, beating well after each additions.\n" +
                    "Combine the sweet potatoes, buttermilk, milk, pecans, vanilla extract, cinnamon and nutmeg in a large bowl and mix well. Add butter mixture to sweet potato mixture and mix well.\n" +
                    "Spoon the sweet potato mixture into prepared baking dish. Bake for 1 1/4 hours. Serve Immediately.', 0, 0)");


            /** Strawberry Blueberry Oatmeal Protein Smoothie  */
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-6,-31,'1/2 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-6,-32,'1/2 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-6,-33,'1/2 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-6,-34,'2 scoops')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-6,-35,'1')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-6,-1,'1 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-6,-7,'1/2 tsp')");
            db.execSQL("INSERT INTO recipe (uid,name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite)" +
                    " VALUES (-6,'Protein Smoothie', 'protein_smoothie', '10 minutes', 1, 'drink', " +
                    "'If using frozen fruit, blend/crush up the fruit. Taking the fruit out, put the oats in the blender and let it run until they are ground up to as close of a powder as desired.\n" +
                    "Add the remaining ingredients and blend until smooth.', 0, 0)");

            /* Crack Chicken Tacos */
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-12,-8,'2 lb')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-12,-58,'1 tsp')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-12,-59,'4')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-12,-60,'juice from 2')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-12,-61,'1/2')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-12,-62,'2 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-12,-38,'1 cup')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-12,-4,'3')");
            db.execSQL("INSERT INTO recipe (uid,name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite)" +
                    " VALUES (-12,'Chicken Tacos', 'crack_chicken_tacos', '50 minutes', 4, 'main', " +
                    "'Make crack chicken: Preheat oven to 425°. In a large resealable plastic bag, combine chicken and flour and shake until fully coated.\n" +
                    "Set up a dredging station: In one bowl, add panko bread crumbs and in another bowl, whisk eggs and add 2 tablespoons water. Dip chicken in eggs then in panko until fully coated. Transfer to a parchment-lined baking sheet and season generously with salt and pepper.\n" +
                    "Bake until golden and crispy, 23 to 25 minutes."+
                    "Meanwhile, in a small saucepan over low heat, warm barbecue sauce, brown sugar, lime juice, and garlic powder.\n"+
                    "Coat baked chicken in sauce.\n" +
                    "Serve chicken in tortillas with cabbage and drizzled with ranch. Garnish with cilantro.', 0, 0)");

            /*S'mores Fudge*/
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-13,-4,'3')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-13,-15,'2 cups')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-13,-17,'2 tsp')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-13,-16,'6 tsp')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-13,-63,'9')");
            db.execSQL("INSERT INTO recipe (uid,name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite)" +
                    " VALUES (-13,'S mores Fudge', 'smores_fudge', '50 minutes', 4, 'main', " +
                    "'Line an 8\"-x-8\" pan with foil and spray with cooking spray. Make graham cracker crust: In a large bowl, stir together graham cracker crumbs, butter, sugar, and salt until completely moist. Press into prepared pan until packed. Freeze while you make fudge layer and frosting.\n" +
                    "Make chocolate fudge: In a small saucepan, combine sweetened condensed milk, chocolate chips, and vanilla.\n" +
                    "Make marshmallow frosting: Place a heatproof bowl over a small saucepan of simmering water over medium heat. Whisk together egg whites, water, light corn syrup, and sugar and cook 3 minutes. Using a hand mixer, whip until glossy."+
                    "Heat broiler. Pour the chocolate fudge layer over the graham cracker crust and smooth to create an even layer. Then, pour the marshmallow frosting on top and smooth. Broil 2 minutes, then freeze 1 hour. Slice into squares and serve.\n', 0, 0)");


            /*Polar Bear Paw Cupcakes*/
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-14,-15,'1 box')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-14,-64,'1 can')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-14,-65,'9')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-14,-66,'72')");
            db.execSQL("INSERT INTO recipe (uid,name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite)" +
                    " VALUES (-14,'Bear Cupcakes', 'bear_cupcakes', '1 hour', 18, 'main', " +
                    "'Preheat oven to 350° and line two cupcake pans with 18 cupcake liners. Prepare cake mix according to package instructions.\n" +
                    "Bake until a toothpick inserted into the center of the cupcakes comes out clean, 20 to 25 minutes. Let cupcakes cool completely.\n" +
                    "Add coconut to a shallow bowl. Using a small offset spatula (or a knife) spread frosting onto the cupcakes then dip the tops in the coconut."+
                    "Twist Oreos apart so that you have 18 halves. Place an Oreo half on top of each cupcake, then place 4 brown M&Ms on top of the Oreos to make the paws.\n', 0, 0)");

            /*Roasted Butternut Squash Soup*/
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-15,-16,'1 tsp')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-15,-68,'1 large')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-15,-3,'2')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-15,-67,'3 tsp')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-15,-41,'1')");
            db.execSQL("INSERT INTO recipeIngredients (recipe_id,ingredient_id,ingredient_amount) VALUES " +
                    "(-15,-44,'1')");
            db.execSQL("INSERT INTO recipe (uid,name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite)" +
                    " VALUES (-15,'Butternut Soup', 'butternut_soup', '1 hour', 18, 'main', " +
                    "'Preheat oven to 400º. On a large baking sheet, toss butternut squash and potatoes with 2 tablespoons olive oil and season generously with salt and pepper. Roast until tender, 25 minutes.\n" +
                    "Meanwhile, in a large pot over medium heat, melt butter and remaining tablespoon olive oil. Add onion, celery, and carrot and cook until softened, 7 to 10 minutes. Season generously with salt, pepper, and thyme.\n" +
                    "Add roasted squash and potatoes and pour over chicken broth. Simmer 10 minutes, then using an immersion blender, blend soup until creamy. (Alternately, carefully transfer batches of the hot soup to a blender.)"+
                    "Serve garnished with thyme.\n', 0, 0)");


            db.execSQL("INSERT INTO user (name, age, picture_path) VALUES ('Name', 'age', 'android.resource://com.tama.mealwhisperer/mipmap/profile')");
            db.execSQL("INSERT INTO recipe (name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite) VALUES ('Mushroom soup', 'mushroom_soup', '20 minutes', 2, 'soup', 'Slice mushrooms and garlic.\nFry mushroom slices and garlic with olive oil.\nOnce mushrooms are cooked, add milk, cream water. Stir.\nAdd vegetable soup cube.\nReduce heat, add pepper and parsley.\nTurn off the stove before the mixture boils.\nBlend the mixture.', 0, 1)");
            db.execSQL("INSERT INTO recipe (name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite) VALUES ('Cheese on Toast', 'cheese_on_toast', '3 minutes', 1, 'appetizer', 'Slice the bread and cheese.\nGrill one side of each slice of bread.\nTurn over the bread and place a slice of cheese on each piece.\nGrill until the cheese has started to melt.\nServe and enjoy!', 0, 1)");
            db.execSQL("INSERT INTO recipe (name, picture_path, preparation_time, portion_count, type, description, user_defined, favorite) VALUES ('Crepes', 'crepes', '30 minutes', 1, 'main', 'Pour the milk into the flour. Stir.\nAdd the oil, the beaten eggs and the sugar. Stir again.\nLet the batter rest for 2 hours. The batter must be fluid. If not, add a little more milk.\nTake a frying pan, oil it and pour a small amount of batter and spread it on the bottom. Cook it on one side, then the other.\nThe \"French Crepes\" must be very thin. You can put butter or sugar or jelly or melted chocolate on them.', 0, 1)");
        }

    };

    public AppDatabase getAppDatabase() {
        return appDatabase;
    }

}
