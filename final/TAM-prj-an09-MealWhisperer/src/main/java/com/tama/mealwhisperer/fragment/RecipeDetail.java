package com.tama.mealwhisperer.fragment;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tama.mealwhisperer.AppDatabase;
import com.tama.mealwhisperer.MealWhisperer;
import com.tama.mealwhisperer.R;
import com.tama.mealwhisperer.entity.Ingredient;
import com.tama.mealwhisperer.entity.Recipe;
import com.tama.mealwhisperer.entity.RecipeIngredients;
import com.tama.mealwhisperer.listener.BitmapStorageSaver;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by teap3 on 26.10.2017.
 */

public class RecipeDetail extends Fragment {
    private Recipe recipe;
    private List<RecipeIngredients> recipeIngredients;
    private List<Ingredient> ingredients;
    private ImageView recipeImageView;
    private TextView recipeNameView;
    private TextView prepTimeView;
    private TextView portionCountView;
    private TextView recipeProcedureView;
    private TextView ingredientsView;
    private final AppDatabase db = MealWhisperer.get().getAppDatabase();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.recipe_detail, container, false);

        this.recipeImageView = rootView.findViewById(R.id.recipe_detail_picture);
        this.recipeNameView = rootView.findViewById(R.id.recipe_name);
        this.prepTimeView = rootView.findViewById(R.id.recipe_prep_time);
        this.portionCountView = rootView.findViewById(R.id.recipe_portion_count);
        this.recipeProcedureView = rootView.findViewById(R.id.recipe_procedure);
        this.ingredientsView = rootView.findViewById(R.id.recipe_ingredients);

        Bundle bundle = getArguments();

        //Get recipe object from previous fragment
        this.recipe = (Recipe) bundle.getSerializable("recipe");

        //Set recipe picture
        // get image from user recipe storage
        if(this.recipe.getPicturePath().startsWith("/data")){
           BitmapStorageSaver saver = new BitmapStorageSaver();
           Bitmap profilePicture = saver.loadRecipeImage(this.recipe.getPicturePath());
           this.recipeImageView.setImageBitmap(profilePicture);
        } else{
           // get pictures of default recipes
           this.recipeImageView.setImageResource(getResources().getIdentifier
                   (this.recipe.getPicturePath(), "drawable", "com.tama.mealwhisperer"));
        }

        //Set recipe name and font
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/dancing_script.ttf");
        this.recipeNameView.setText(recipe.getName());
        this.recipeNameView.setTypeface(typeface);

        //Set preparation time
        this.prepTimeView.setText(recipe.getPrepTime());
        this.prepTimeView.getBackground().setAlpha(90);

        //Set portion count
        Integer portionCount = recipe.getPortionCount();
        if (portionCount > 1)
            this.portionCountView.setText(portionCount.toString() + " portions");
        else
            this.portionCountView.setText(portionCount.toString() + " portion");


        //Set recipe procedure steps:
        setProcedureStepsText();

        setIngredientsList();

        return rootView;

    }

    private void setProcedureStepsText() {
        //Split text to lines
        String lines[] = this.recipe.getDescription().split("\\r?\\n");

        for (Integer i = 0; i < lines.length; i++) {
            lines[i] = ((Integer) (i + 1)).toString() + ". " + lines[i];
            if (i != lines.length - 1)
                lines[i] += "\n\n";
        }

        StringBuilder builder = new StringBuilder();
        for (String s : lines) {
            builder.append(s);
        }

        this.recipeProcedureView.setText(builder.toString());

    }

    private void setIngredientsList() {
        ArrayList<Integer> ingredientsId = new ArrayList<>();

        //Get ingredients id from database
        this.recipeIngredients =
                db.recipeIngredientsDao().findRecipeIngredientsById(this.recipe.getUid());


        for (RecipeIngredients r : this.recipeIngredients) //Get ingredients IDs
            ingredientsId.add(r.getIngredientId());

        this.ingredients = db.ingredientDao().loadAllByIds(
                ingredientsId.toArray(new Integer[ingredientsId.size()]));

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < this.ingredients.size(); i++) {
            RecipeIngredients rcping = getRecipeIngredientById(this.ingredients.get(i).getUid());
            builder.append("&#8226; " + this.ingredients.get(i).getName() + ", " + rcping.ingredientAmount);
            if (i != this.ingredients.size() - 1)
                builder.append("<br/><br/>");
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N)
            this.ingredientsView.setText(Html.fromHtml(builder.toString(), Html.FROM_HTML_MODE_LEGACY));
        else
            this.ingredientsView.setText(Html.fromHtml(builder.toString()));


    }

    private RecipeIngredients getRecipeIngredientById(Integer id) {
        for (RecipeIngredients i : this.recipeIngredients) {
            if (i.getIngredientId() == id)
                return i;
        }

        return null;
    }

    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    Bundle bundle = getArguments();
                    if (bundle.containsKey("fromProfilePage"))
                    {
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        ProfileFragment profileFragment = new ProfileFragment();

                        ft.replace(R.id.layout_fragment, profileFragment).commit();
                        return true;
                    }else if(bundle.containsKey("fromSwipePage")){
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

                        SwipeRecipeFragment swipeRecipeFragment = (SwipeRecipeFragment) bundle.get("SwipeRecipeFragment");
                        swipeRecipeFragment.setBackPressed(true);

                        ft.replace(R.id.layout_fragment,swipeRecipeFragment).commit();
                        return  true;
                    }
                }

                return false;
            }
        });

    }
}