package com.tama.mealwhisperer.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.tama.mealwhisperer.entity.User;

/**
 * Created by norbert.durcansky on 10/8/2017.
 */

@Dao
public interface UserDao {

    @Query("SELECT * FROM user LIMIT 1")
    User getUser();

    @Query("SELECT COUNT(*) FROM user")
    Integer getUsersCount();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUser(User user);

    @Update
    public void updateUser(User user);

    @Delete
    void deleteUser(User user);
}