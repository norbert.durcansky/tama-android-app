package com.tama.mealwhisperer.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

/**
 * Created by norbert.durcansky on 10/8/2017.
 */


//Have to implement serializable so we could pass in into bundle (passing data between fragments)
@Entity
public class Recipe implements Serializable{

    @PrimaryKey(autoGenerate = true)
    private Integer uid;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "picture_path")
    private String picturePath;

    @ColumnInfo(name = "preparation_time")
    private String prepTime;

    @ColumnInfo(name = "portion_count")
    private Integer portionCount;

    //TODO converter
    @ColumnInfo(name = "type")
    private String type;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "user_defined")
    private Boolean userDefined;

    @ColumnInfo(name = "favorite")
    private Boolean favorite;

    @Ignore
    private Double MatchRate=0.0;

    public Double getMatchRate() {
        return MatchRate;
    }

    public void setMatchRate(Double matchRate) {
        MatchRate = matchRate;
    }
    public void setMatchRate() {
        MatchRate+=1;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Recipe setUidO(Integer uid) {
        this.uid = uid;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Recipe setNameO(String name) {
        this.name = name;
        return this;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public Recipe setPicturePathO(String picturePath) {
        this.picturePath = picturePath;
        return this;
    }


    public String getPrepTime() {
        return prepTime;
    }
    public void setPrepTime(String prepTime) {
        this.prepTime = prepTime;
    }

    public Recipe setPrepTimeO(String prepTime) {
        this.prepTime = prepTime;
        return this;
    }

    public Integer getPortionCount() {
        return portionCount;
    }

    public void setPortionCount(Integer portionCount) {
        this.portionCount = portionCount;
    }

    public Recipe setPortionCountO(Integer portionCount) {
        this.portionCount = portionCount;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Recipe setTypeO(String type) {
        this.type = type;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Recipe setDescriptionO(String description) {
        this.description = description;
        return this;
    }

    public Boolean getUserDefined() {return userDefined;}
    public void setUserDefined(Boolean userDefined) {this.userDefined = userDefined;}

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public Recipe setFavoriteO(Boolean favorite) {
        this.favorite = favorite;
        return this;
    }
}
