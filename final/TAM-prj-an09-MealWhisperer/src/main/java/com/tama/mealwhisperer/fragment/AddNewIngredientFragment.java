package com.tama.mealwhisperer.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tama.mealwhisperer.AppDatabase;
import com.tama.mealwhisperer.MainActivity;
import com.tama.mealwhisperer.MealWhisperer;
import com.tama.mealwhisperer.R;
import com.tama.mealwhisperer.entity.Ingredient;

/**
 * Created by hpros on 7. 12. 2017.
 */

public class AddNewIngredientFragment extends Fragment {
    private View view;
    private Ingredient newIngredient = new Ingredient();
    private final AppDatabase db = MealWhisperer.get().getAppDatabase();
    private AddListFragment addListFragment;

    public void setAddListFragment(AddListFragment listFragment){ this.addListFragment = listFragment;}

    public void addNewIngredient(){
        final Button button = (Button) view.findViewById(R.id.add_ingredient_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText temp = view.findViewById(R.id.new_ingredient_name);
                String name = temp.getText().toString();
                if(name.equals("")){
                    Toast.makeText(MainActivity.getContextOfApplication(), "Fill in ingredient name.",
                            Toast.LENGTH_SHORT).show();
                } else{
                    newIngredient.setName(name);

                    db.ingredientDao().insert(newIngredient);

                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.layout_fragment, addListFragment).commit();
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.add_new_ingredient, container, false);
        view = rootView;
        addNewIngredient();
        return rootView;
    }
}
