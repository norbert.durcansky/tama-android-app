package com.tama.mealwhisperer.listener;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.tama.mealwhisperer.MainActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by teap3 on 24.10.2017.
 */

/**
 * Class that stores profile images to internal storage
 */
public class BitmapStorageSaver {

    public String pictureName = "profile.jpg";

    public void setPictureName(String name){
        pictureName = name;
    }

    public String getPictureName(){
        return pictureName;
    }

    public String saveBitmap(Bitmap bitmapImage){
        FileOutputStream outputStream = null;

        ContextWrapper cw = new ContextWrapper(MainActivity.getContextOfApplication());

        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

        // Create image directory
        File filePath = new File(directory, this.pictureName);

        try {

            outputStream = new FileOutputStream(filePath); //Save image to internal storage
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            try {
                //Close the output stream
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    public Bitmap loadImageFromStorage(String path)
    {

        try {
            File file = new File(path, pictureName);
            return BitmapFactory.decodeStream(new FileInputStream(file));
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            return null;
        }

    }

    public Bitmap loadRecipeImage(String path){
        try {
            File file = new File(path);
             return BitmapFactory.decodeStream(new FileInputStream(file));
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
