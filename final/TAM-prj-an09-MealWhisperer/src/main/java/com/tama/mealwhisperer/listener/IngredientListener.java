package com.tama.mealwhisperer.listener;

import com.tama.mealwhisperer.entity.Ingredient;

/**
 * Created by norbert.durcansky on 10/21/2017.
 */

/**
 * Adapter callbacks for ingredients
 * notifies parent fragment
 */
public interface IngredientListener {
    void onAddIngredientAdapter(Ingredient ingredient);
    void onRemoveIngredientAdapter(Ingredient ingredient);
    void onRemoveAddIngredientAdapter(Ingredient ingredient);
}
