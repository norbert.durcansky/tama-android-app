package com.tama.mealwhisperer;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.tama.mealwhisperer.dao.IngredientDao;
import com.tama.mealwhisperer.dao.RecipeDao;
import com.tama.mealwhisperer.dao.RecipeIngredientsDao;
import com.tama.mealwhisperer.dao.UserDao;
import com.tama.mealwhisperer.entity.Ingredient;
import com.tama.mealwhisperer.entity.Recipe;
import com.tama.mealwhisperer.entity.RecipeIngredients;
import com.tama.mealwhisperer.entity.User;

/**
 * Created by norbert.durcansky on 10/8/2017.
 */

/** define your entities here*/
@Database(entities = {User.class, Ingredient.class, Recipe.class, RecipeIngredients.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    /** define your daos here */
    public abstract UserDao userDao();
    public abstract IngredientDao ingredientDao();
    public abstract RecipeDao recipeDao();
    public abstract RecipeIngredientsDao recipeIngredientsDao();
}