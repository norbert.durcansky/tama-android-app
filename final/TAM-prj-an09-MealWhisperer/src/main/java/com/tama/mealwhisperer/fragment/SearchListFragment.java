package com.tama.mealwhisperer.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.tama.mealwhisperer.MealWhisperer;
import com.tama.mealwhisperer.R;
import com.tama.mealwhisperer.adapter.IngredientListAdapter;
import com.tama.mealwhisperer.entity.Ingredient;
import com.tama.mealwhisperer.listener.IngredientListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by norbert.durcansky on 10/4/2017.
 */


public class SearchListFragment extends Fragment implements IngredientListener {

    RecyclerView recyclerView;
    public IngredientListAdapter ingredientListAdapter;
    IngredientListener ingredientListener;
    public List<Ingredient> ingredientList = new ArrayList<>();
    public List<Ingredient> selectedIngredients = new ArrayList<>();
    SearchSelectedFragment searchSelectedFragment;

    /* getters setters */

    public void setSearchSelectedFragment(SearchSelectedFragment searchSelectedFragment) {
        this.searchSelectedFragment = searchSelectedFragment;
    }

    public List<Ingredient> getSelectedIngredients() {
        return selectedIngredients;
    }

    public void setSelectedIngredients(List<Ingredient> selectedIngredients) {
        this.selectedIngredients = selectedIngredients;
    }

    public List<Ingredient> getIngredientList() {
        return ingredientList;
    }

    public IngredientListAdapter getIngredientListAdapter() {
        return ingredientListAdapter;
    }

    public void setIngredientListAdapter(IngredientListAdapter ingredientListAdapter) {
        this.ingredientListAdapter = ingredientListAdapter;
    }

    public void setIngredientList(List<Ingredient> ingredientList) {
        this.ingredientList = ingredientList;
    }
    /* */

    /** interface to get data from adapter */
    public interface IngredientListener {
        void onAddIngredient(Ingredient ingredient);
        void onSearchBackButtonPressed();
    }

    /**
     * Adapter callback
     * @param ingredient
     */
    @Override
    public void onAddIngredientAdapter(Ingredient ingredient) {
        ingredientListener.onAddIngredient(ingredient);
        ingredientList.remove(ingredient);
        selectedIngredients.add(ingredient);
        ingredientListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRemoveAddIngredientAdapter(Ingredient ingredient){

    }

    @Override
    public void onRemoveIngredientAdapter(Ingredient ingredient) {

    }

    /** callback to activity */
    @Override
    public void onAttach(Context context) {
        ingredientListener = (IngredientListener) context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView =  inflater.inflate(R.layout.search_list_fragment, container, false);

        /** init recycler view */
        recyclerView = (RecyclerView)rootView.findViewById(R.id.ingredients_list);
        ingredientListAdapter = new IngredientListAdapter(ingredientList,this.getContext(),this);
        recyclerView.setAdapter(ingredientListAdapter);

        /** init toolbar with back button */
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /** callback to activity for changing fragment */
                ingredientListener.onSearchBackButtonPressed();
            }
        });

        /** load data from DB and filter selected */
        ingredientList.addAll(MealWhisperer.get().getAppDatabase().ingredientDao().getAll());

        /** filter already selected */
        for(Ingredient selectedIngredient:selectedIngredients){
            if(ingredientList.contains(selectedIngredient)){
                ingredientList.remove(selectedIngredient);
            }
        }

        SearchView searchView = (SearchView)rootView.findViewById(R.id.search);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                ingredientListAdapter.filter(query,selectedIngredients);
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                ingredientListAdapter.filter(newText,selectedIngredients);
                return false;
            }
        });

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK ) {
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.layout_fragment, searchSelectedFragment).commit();
                    return true;
                }
                return false;
            }
        });

    }
}