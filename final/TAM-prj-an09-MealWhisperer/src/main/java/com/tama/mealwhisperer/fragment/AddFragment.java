package com.tama.mealwhisperer.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.tama.mealwhisperer.AppDatabase;
import com.tama.mealwhisperer.MainActivity;
import com.tama.mealwhisperer.MealWhisperer;
import com.tama.mealwhisperer.R;
import com.tama.mealwhisperer.adapter.AddIngredientAdapter;
import com.tama.mealwhisperer.entity.Ingredient;
import com.tama.mealwhisperer.entity.Recipe;
import com.tama.mealwhisperer.entity.RecipeIngredients;
import com.tama.mealwhisperer.listener.BitmapStorageSaver;
import com.tama.mealwhisperer.listener.IngredientListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by norbert.durcansky on 10/4/2017.
 */

public class AddFragment extends Fragment implements IngredientListener {
    RecyclerView recyclerSelectedView;
    IngredientListener ingredientListener;
    AddIngredientAdapter addIngredientAdapter;
    private ImageView recipePictureView;
    private final int IMAGE_GALLERY_REQUEST = 20;
    private final int IMAGE_CAMERA = 21;
    private String pictureDirectoryPath = "android.resource://com.tama.mealwhisperer/mipmap/meal";
    private final AppDatabase db = MealWhisperer.get().getAppDatabase();
    private View view;
    private Bitmap image;
    private int finished = 0;
    public int numberOfLines = 0;
    private Bundle savedState = null;

    public List<Ingredient> selectedIngredients = new ArrayList<>();

    public List<Ingredient> getSelectedIngredients() {
        return selectedIngredients;
    }

    public void setSelectedIngredients(List<Ingredient> selectedIngredients) {
        this.selectedIngredients = selectedIngredients;
    }

    public interface IngredientListener {
        void onAddRemoveIngredient(Ingredient ingredient);
    }

    @Override
    public void onAddIngredientAdapter(Ingredient ingredient) {

    }

    @Override
    public void onRemoveIngredientAdapter(Ingredient ingredient) {

    }


    @Override
    public void onRemoveAddIngredientAdapter(Ingredient ingredient) {
        ingredientListener.onAddRemoveIngredient(ingredient);
        selectedIngredients.remove(ingredient);
        addIngredientAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        ingredientListener = (AddFragment.IngredientListener) context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.add_fragment, container, false);
        view = rootView;
        finished = 0;

        recyclerSelectedView = (RecyclerView) rootView.findViewById(R.id.add_selected_ingredients);
        addIngredientAdapter = new AddIngredientAdapter(selectedIngredients, this);
        recyclerSelectedView.setAdapter(addIngredientAdapter);

        recipePictureView = (ImageView) rootView.findViewById(R.id.recipeImageView);

        setRecipePicture();
        setImageHandler();
        addRecipeDirectionEditText();

        if (savedInstanceState != null || savedState != null) {
            super.onCreate(savedInstanceState);
            pictureDirectoryPath = savedState.getString("file_path");

            Integer num = savedState.getInt("numberOfLines");
            numberOfLines = 0;
            for (Integer i = 0; i<num; i++){
                addLine();
            }
            numberOfLines = num;

            ArrayList<String> list = savedState.getStringArrayList("directions");
            EditText temp;
            for (Integer i = 1; i < numberOfLines; i++){
                int resID = getResources().getIdentifier(i.toString(), "id", getActivity().getPackageName());
                    temp = view.findViewById(resID);
                    String line = list.get(i);
                    temp.setText(line);
            }

            image = savedState.getParcelable("image");
            this.recipePictureView.setImageBitmap(image);
            setRecipePicture();
        }
        return rootView;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        savedState = saveState();
    }

    private Bundle saveState(){
        if(finished == 0){
            Bundle savedInstanceState = new Bundle();
            savedInstanceState.putInt("numberOfLines", numberOfLines);
            ArrayList<String> list = new ArrayList<String>();

            savedInstanceState.putString("file_path", pictureDirectoryPath);
            savedInstanceState.putParcelable("image", image);

            EditText temp;
            String line;
            for (Integer i = 1; i <= numberOfLines; i++){
                int resID = getResources().getIdentifier(i.toString(), "id", getActivity().getPackageName());
                temp = view.findViewById(resID);
                line = temp.getText().toString();
                list.add(line);

            }
            savedInstanceState.putStringArrayList("directions", list);

            return savedInstanceState;
        }
        else{
            Bundle savedInstanceState = new Bundle();
            savedInstanceState.putInt("numberOfLines", 0);
            ArrayList<String> list = new ArrayList<String>();

            savedInstanceState.putString("file_path", "android.resource://com.tama.mealwhisperer/mipmap/meal");
            savedInstanceState.putParcelable("image", null);

            return savedInstanceState;
        }
    }

    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putBundle("SAVEDSTATE", (savedState != null) ? savedState : saveState());
    }

    public void addRecipeDirectionEditText(){
        final ImageButton button = (ImageButton) view.findViewById(R.id.direction_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addLine();
            }
        });
    }

    public void addLine() {
        LinearLayout ll = (LinearLayout) view.findViewById(R.id.linearLayoutDirections);
        // add edittext
        EditText et = new EditText(this.getContext());
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        et.setLayoutParams(p);
        et.setHint("Direction");
        et.setId(numberOfLines + 1);
        ll.addView(et);
        numberOfLines++;
    }


    private void setRecipePicture() {
        if (pictureDirectoryPath.startsWith("android.resource")) //Default profile picture
        {
            recipePictureView.setImageURI(null);
            recipePictureView.setImageURI(Uri.parse(pictureDirectoryPath));
        } else //if user changed his profile photo
        {
            BitmapStorageSaver saver = new BitmapStorageSaver();
            InputStream inputStream = null;
            try {
                inputStream = getContext().getContentResolver().openInputStream(Uri.parse(pictureDirectoryPath));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            Bitmap profilePicture = BitmapFactory.decodeStream(inputStream);
            this.recipePictureView.setImageBitmap(profilePicture);
        }
    }


    private void setImageHandler()
    {
        recipePictureView.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                CharSequence options[] = new CharSequence[] {/*"Take a photo",*/
                        "Choose from Gallery"};

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Select image source:");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent photoIntent = new Intent(Intent.ACTION_PICK); //Create Intent for action
                        File pictureDirectory = Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES); //Get pictureDirectory

                        if(pictureDirectory.getPath().length() != 0) {
                            pictureDirectoryPath = pictureDirectory.getPath(); //Convert it to string
                            Uri parsedUri = Uri.parse(pictureDirectoryPath);
                            photoIntent.setDataAndType(parsedUri, "image/*"); //Set that we are looking for images
                            startActivityForResult(photoIntent, IMAGE_GALLERY_REQUEST); //Start activity
                        }
                    }
                });
                builder.show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Check for result code and request code
        if (resultCode != RESULT_OK)
            return;

        if (requestCode != IMAGE_GALLERY_REQUEST && requestCode != IMAGE_CAMERA)
            return;

        Context appContext = MainActivity.getContextOfApplication();
        InputStream inputStream;

        try {
            if (requestCode == IMAGE_GALLERY_REQUEST) //From gallery
            {
                Uri chosenImageUri = data.getData(); //Get URI of picture chosen by user
                inputStream = appContext.getContentResolver().openInputStream(chosenImageUri);
                pictureDirectoryPath = chosenImageUri.toString();
                image = BitmapFactory.decodeStream(inputStream); //Convert stream to bitmap and set new profile picture
                recipePictureView.setImageBitmap(image);
            }
        } catch (FileNotFoundException e)
        {
            Toast.makeText(appContext, "File not found.", Toast.LENGTH_LONG).show();
        }
    }

    public int getValuesForRecipe(Recipe recipe, int numberOfLines){
        String str = "";
        String recipeImgPath;

        // get recipe name
        EditText temp = view.findViewById(R.id.recipeName);
        str = temp.getText().toString();

        if (str.length() == 0){
            return 1;
        } else {
            recipe.setName(str);
            recipeImgPath = str;
        }

        temp = view.findViewById(R.id.preparation_time);
        str = temp.getText().toString();

        if (str.length() == 0){
            return 1;
        } else {
            recipe.setPrepTime(str);
        }

        temp = view.findViewById(R.id.portion_count);
        str = temp.getText().toString();

        if (str.length() == 0){
            return 1;
        } else {
            int count;
            try{
                count = Integer.parseInt(str);
            }catch(NumberFormatException e){
                Toast.makeText(MainActivity.getContextOfApplication(), "Portion count must be a number.",
                        Toast.LENGTH_SHORT).show();
                return  1;
            }
            recipe.setPortionCount(count);
        }

        String directions = "";
        String line;

        for (Integer i = 1; i <= numberOfLines; i++){
            int resID = getResources().getIdentifier(i.toString(), "id", getActivity().getPackageName());
            temp = view.findViewById(resID);
            line = temp.getText().toString();
            if (i == numberOfLines)
                directions += line;
            else
                directions += line + "\n";
        }

        if (directions.length() == 0) {
            return 3;
        } else {
            recipe.setDescription(directions);
        }

        if(image == null){
            return 2;
        }

        BitmapStorageSaver saver = new BitmapStorageSaver();

            saver.setPictureName(recipeImgPath);
            String path = saver.saveBitmap(image);
            System.out.print(path);
            path = path + "/" + recipeImgPath;
            recipe.setPicturePath(path);

        recipe.setFavorite(true);

        if(selectedIngredients.size() == 0){
            return 4;
        }

        return 0;
    }

    public void getValuesForRecipeIngredients(RecipeIngredients recipeIngredient, long recipeID){
        Integer id;
        EditText temp;
        String line;
        List<String> amounts = new ArrayList<>();

        RecyclerView recyclerView;
        recyclerView = view.findViewById(R.id.add_selected_ingredients);
        for (int i = 0; i < recyclerView.getChildCount(); i++){
            View child = recyclerView.getChildAt(i);
            EditText text = child.findViewById(R.id.amount);
            String amount = text.getText().toString();
            amounts.add(amount);
        }

        for (Integer i = 0; i < selectedIngredients.size(); i++){
            id = selectedIngredients.get(i).getUid();
            recipeIngredient.setRecipeId((int)recipeID);
            recipeIngredient.setIngredientId(id);
            recipeIngredient.setIngredientAmount(amounts.get(i));

            db.recipeIngredientsDao().insertRecipeIngredients(recipeIngredient);
        }
    }

    public void resetFields(){
        EditText temp = view.findViewById(R.id.recipeName);
        temp.setText("");

        temp = view.findViewById(R.id.preparation_time);
        temp.setText("");

        temp = view.findViewById(R.id.portion_count);
        temp.setText("");

        pictureDirectoryPath = "android.resource://com.tama.mealwhisperer/mipmap/meal";

        numberOfLines = 0;
    }

    public void insertNewRecipe(){

        Recipe newRecipe = new Recipe();
        RecipeIngredients recipeIngredients = new RecipeIngredients();

        int allOk = getValuesForRecipe(newRecipe, numberOfLines);

        if(allOk == 2){
            Toast.makeText(MainActivity.getContextOfApplication(), "Please add photo.",
                    Toast.LENGTH_SHORT).show();
        }else if (allOk == 3){
            Toast.makeText(MainActivity.getContextOfApplication(), "Fill in directions please.",
                    Toast.LENGTH_SHORT).show();
        }else if (allOk == 4){
            Toast.makeText(MainActivity.getContextOfApplication(), "Please add ingredients.",
                    Toast.LENGTH_SHORT).show();
        }else if (allOk != 0){
            Toast.makeText(MainActivity.getContextOfApplication(), "Fill in all fields please.",
                    Toast.LENGTH_SHORT).show();
        }else {
            long newID = db.recipeDao().insertRecipe(newRecipe);
            getValuesForRecipeIngredients(recipeIngredients, newID);

            newRecipe.setUid((int)newID);

            Toast.makeText(MainActivity.getContextOfApplication(), "Recipe was successfully added.",
                    Toast.LENGTH_SHORT).show();

            Bundle bundle = new Bundle();
            bundle.putSerializable("recipe", newRecipe);
            bundle.putString("fromAddRecipe", "");

            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            RecipeDetail recipeDetail = new RecipeDetail();
            recipeDetail.setArguments(bundle);

            finished = 1;
            resetFields();
            ft.replace(R.id.layout_fragment, recipeDetail).commit();
        }
    }
}