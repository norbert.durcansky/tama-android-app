package com.tama.mealwhisperer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.tama.mealwhisperer.R;
import com.tama.mealwhisperer.entity.Ingredient;
import com.tama.mealwhisperer.listener.IngredientListener;

import java.util.List;

/**
 * Created by norbert.durcansky on 10/9/2017.
 */

public class IngredientSelectedAdapter extends RecyclerView.Adapter<IngredientSelectedAdapter.IngredientViewHolder> {

    private  List<Ingredient> list;

    private IngredientListener mAdapterCallback;


    public IngredientSelectedAdapter(List<Ingredient> list, Context mContext,IngredientListener callback) {
        this.list=list;
        /* callback to send data to MainActivity */
        this.mAdapterCallback=callback;
    }

    @Override
    public IngredientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ingredient_selected_view, parent, false);
        return new IngredientViewHolder(view);
    }

    @Override
    public void onBindViewHolder(IngredientViewHolder holder, int position) {
        holder.bind(list.get(position));
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAdapterCallback.onRemoveIngredientAdapter(list.get((int)view.getTag()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class IngredientViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private Button button;

        public IngredientViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.ingredient_name);
            button = itemView.findViewById(R.id.ingredient_remove);
        }

        public void bind(Ingredient ingredient) {
            name.setText(ingredient.getName());
            button.setTag(getAdapterPosition());
        }
    }
}