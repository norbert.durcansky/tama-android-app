package com.tama.mealwhisperer.fragment;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lorentzos.flingswipe.SwipeFlingAdapterView;
import com.tama.mealwhisperer.MealWhisperer;
import com.tama.mealwhisperer.R;
import com.tama.mealwhisperer.adapter.SwipeAdapter;
import com.tama.mealwhisperer.entity.Ingredient;
import com.tama.mealwhisperer.entity.Recipe;
import com.tama.mealwhisperer.service.RecipeService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import at.markushi.ui.CircleButton;

/**
 * Created by norbert.durcansky on 10/29/2017.
 */

public class SwipeRecipeFragment extends Fragment implements Serializable {

    SearchListFragment.IngredientListener ingredientListener;
    List<Ingredient> selectedIngredients = new ArrayList<>();
    List<Recipe> matchedRecipes = new ArrayList<>();
    SearchSelectedFragment searchSelectedFragment;
    SwipeRecipeFragment that = this;
    Boolean backPressed = false;

    public void setSearchSelectedFragment(SearchSelectedFragment searchSelectedFragment) {
        this.searchSelectedFragment = searchSelectedFragment;
    }

    public void setSelectedIngredients(List<Ingredient> selectedIngredients) {
        this.selectedIngredients = selectedIngredients;
    }

    public void setBackPressed(Boolean backPressed) {
        this.backPressed = backPressed;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ingredientListener = (SearchListFragment.IngredientListener) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.recipes_swipe_fragment, container, false);

        /** calculate matchRate and get recipes*/
        if(!backPressed) {
            matchedRecipes = RecipeService.getSortedRecipes(selectedIngredients);
        }
        //favorite star implementation
        final CircleButton circleButton = (CircleButton) rootView.findViewById(R.id.addFavorite);

        if(matchedRecipes.size()!=0 && matchedRecipes.get(0).getFavorite()) {
            circleButton.setImageResource(R.drawable.star_on);
            circleButton.setTag(true);
        }else{
            circleButton.setImageResource(R.drawable.star);
            circleButton.setTag(false);
        }
        circleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if((Boolean) circleButton.getTag()){
                    circleButton.setImageResource(R.drawable.star);
                    circleButton.setTag(false);
                    Recipe recipeOff = matchedRecipes.get(0);
                    recipeOff.setFavorite(false);
                    MealWhisperer.get().getAppDatabase().recipeDao().updateRecipe(recipeOff);
                }else{
                    circleButton.setImageResource(R.drawable.star_on);
                    circleButton.setTag(true);
                    Recipe recipeOn = matchedRecipes.get(0);
                    recipeOn.setFavorite(true);
                    MealWhisperer.get().getAppDatabase().recipeDao().updateRecipe(recipeOn);
                    ///
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("recipe", recipeOn);
                    bundle.putSerializable("fromSwipePage", true);
                    bundle.putSerializable("SwipeRecipeFragment", that);
                    //Go to recipe detail fragment
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    RecipeDetail recipeDetail = new RecipeDetail();
                    recipeDetail.setArguments(bundle);
                    ft.replace(R.id.layout_fragment, recipeDetail).commit();
                }

            }
        });

        //swipe functions
        SwipeFlingAdapterView flingContainer = (SwipeFlingAdapterView) rootView.findViewById(R.id.swipe_frame);

        final SwipeAdapter adapter = new SwipeAdapter(rootView.getContext(), matchedRecipes);
        setRecipeName(rootView);

        flingContainer.setAdapter(adapter);
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void onLeftCardExit(Object o) {
            }

            @Override
            public void onRightCardExit(Object o) {
            }

            @Override
            public void onAdapterAboutToEmpty(int i) {
            }

            @Override
            public void onScroll(float v) {
            }

            @Override
            public void removeFirstObjectInAdapter() {

                Recipe recipe = matchedRecipes.get(0);
                matchedRecipes.remove(0);
                matchedRecipes.add(recipe);
                adapter.notifyDataSetChanged();
                setRecipeName(rootView);
                final CircleButton circleButton = (CircleButton) rootView.findViewById(R.id.addFavorite);
                if(matchedRecipes.size()!=0 && matchedRecipes.get(0).getFavorite()) {
                    circleButton.setImageResource(R.drawable.star_on);
                    circleButton.setTag(true);
                }else{
                    circleButton.setImageResource(R.drawable.star);
                    circleButton.setTag(false);
                }
            }
        });

        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {

            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {
                Recipe recipe = (Recipe) matchedRecipes.get(itemPosition);

                Bundle bundle = new Bundle();
                bundle.putSerializable("recipe", recipe);
                bundle.putSerializable("fromSwipePage", true);
                bundle.putSerializable("SwipeRecipeFragment", that);
                //Go to recipe detail fragment
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

                RecipeDetail recipeDetail = new RecipeDetail();
                recipeDetail.setArguments(bundle);

                ft.replace(R.id.layout_fragment, recipeDetail).commit();
            }
        });

        return rootView;
    }


    private void setRecipeName(View rootView) {
        if (matchedRecipes.size() != 0) {
            TextView recipeName = (TextView) rootView.findViewById(R.id.recipe_name);
            //Set recipe name and font
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/dancing_script.ttf");
            recipeName.setText(matchedRecipes.get(0).getName());
            recipeName.setTypeface(typeface);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK && !backPressed) {
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.layout_fragment, searchSelectedFragment).commit();
                    return true;
                } else {
                    backPressed = false;
                }
                return false;
            }
        });

    }


}
