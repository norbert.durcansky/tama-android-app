package com.tama.mealwhisperer.service;

import com.tama.mealwhisperer.MealWhisperer;
import com.tama.mealwhisperer.entity.Ingredient;
import com.tama.mealwhisperer.entity.Recipe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by norbert.durcansky on 10/29/2017.
 */

public final class RecipeService {

    public static List<Recipe> getSortedRecipes(List<Ingredient> selectedIngredients){

        List<Recipe> matchedRecipes =MealWhisperer.get().getAppDatabase().recipeDao().getAllRecipes();
        if(matchedRecipes==null){
            return  new ArrayList<Recipe>();
        }

        Double ingredientNumber;
        for(int i=0;i<matchedRecipes.size();i++){

            List<Ingredient> recipeIngredients = MealWhisperer.get().getAppDatabase().ingredientDao().getIngredientsForRecipe(matchedRecipes.get(i).getUid());
            /** save number of ingredients */
            ingredientNumber = (double)recipeIngredients.size();

            List<Ingredient> tmpList = new ArrayList<>();
            for(Ingredient recipeIngredient: recipeIngredients){
                if(selectedIngredients.contains(recipeIngredient)){
                   tmpList.add(recipeIngredient);
                }
            }
            recipeIngredients.clear();
            recipeIngredients.addAll(tmpList);
            /** save match rate */
            if(recipeIngredients.size()!=0) {
                matchedRecipes.get(i).setMatchRate(((double) recipeIngredients.size())/ingredientNumber);
            }
        }

        Collections.sort(matchedRecipes, new Comparator<Recipe>() {
            @Override
            public int compare(Recipe recipe, Recipe t1) {
                return t1.getMatchRate().compareTo(recipe.getMatchRate());
            }
        });
    return matchedRecipes;
    }
}
