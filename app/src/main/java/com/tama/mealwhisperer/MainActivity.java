package com.tama.mealwhisperer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.tama.mealwhisperer.entity.Ingredient;
import com.tama.mealwhisperer.fragment.AddFragment;
import com.tama.mealwhisperer.fragment.AddListFragment;
import com.tama.mealwhisperer.fragment.AddNewIngredientFragment;
import com.tama.mealwhisperer.fragment.ProfileFragment;
import com.tama.mealwhisperer.fragment.SearchListFragment;
import com.tama.mealwhisperer.fragment.SearchSelectedFragment;
import com.tama.mealwhisperer.fragment.SwipeRecipeFragment;

public class MainActivity extends AppCompatActivity implements SearchListFragment.IngredientListener,SearchSelectedFragment.IngredientListener, AddListFragment.IngredientListener, AddFragment.IngredientListener{

    SearchSelectedFragment searchSelectedFragment = new SearchSelectedFragment();
    ProfileFragment profileFragment = new ProfileFragment();
    SearchListFragment searchListFragment = new SearchListFragment();
    AddFragment addFragment = new AddFragment();
    AddListFragment addListFragment = new AddListFragment();
    SwipeRecipeFragment swipeRecipeFragment = new SwipeRecipeFragment();
    AddNewIngredientFragment addNewIngredient = new AddNewIngredientFragment();

    private static Context appContext;

    public static Context getContextOfApplication()
    {
        return appContext;
    }

    /** callback for back button in searchListFragment */
    @Override
    public void onSearchBackButtonPressed() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.layout_fragment,searchSelectedFragment).commit();
    }

    @Override
    public void onShowAddFragment() {
        searchListFragment.setSearchSelectedFragment(searchSelectedFragment);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.layout_fragment,searchListFragment).commit();
    }

    @Override
    public void onShowSearchFragment() {
        if(searchListFragment.getSelectedIngredients().size()==0){
            Toast toast = Toast.makeText(getApplicationContext(), "No ingredients selected", Toast.LENGTH_SHORT);
            toast.show();
            return;
        }
        swipeRecipeFragment.setSelectedIngredients(searchSelectedFragment.getSelectedIngredients());
        swipeRecipeFragment.setSearchSelectedFragment(searchSelectedFragment);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.layout_fragment,swipeRecipeFragment).commit();

    }

    /** searchFragment List callbacks */

    @Override
    public void onRemoveIngredient(Ingredient ingredient) {
        /* remove from selected ingredients */
        searchListFragment.getSelectedIngredients().remove(ingredient);
    }

    @Override
    public void onAddIngredient(Ingredient ingredient) {
        /* add to selected ingredients */
        searchSelectedFragment.getSelectedIngredients().add(ingredient);
    }

    public void showAddIngredientFragment(View v){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        addListFragment.setAddFragment(addFragment);
        ft.replace(R.id.layout_fragment,addListFragment).commit();
    }

    /** callback for back button in addFragment */

    @Override
    public void onAddIngredientBackButtonPressed() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.layout_fragment,addFragment).commit();
    }

    @Override
    public void onAddIngredientNewRecipe(Ingredient ingredient) {
        /* add to selected ingredients */
        addFragment.getSelectedIngredients().add(ingredient);
    }

    @Override
    public void onAddRemoveIngredient(Ingredient ingredient) {
        // remove from selected ingredients
        addListFragment.getSelectedIngredients().remove(ingredient);
    }

    public void showAddNewIngredient(View w){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        addNewIngredient.setAddListFragment(addListFragment);
        ft.replace(R.id.layout_fragment,addNewIngredient).commit();
    }

    public void addNewRecipe(View v) {
        addFragment.insertNewRecipe();
    }
    /***/

    /**
     * Navigation menu handler
     */
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_search:
                    ft.replace(R.id.layout_fragment, searchSelectedFragment).commit();
                    return true;
                case R.id.navigation_add:
                    ft.replace(R.id.layout_fragment,addFragment).commit();
                    return true;
                case R.id.navigation_profile:
                    ft.replace(R.id.layout_fragment,profileFragment).commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appContext = getApplicationContext();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        /* initial fragment search */
        ft.replace(R.id.layout_fragment,new SearchSelectedFragment()).commit();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }

}
