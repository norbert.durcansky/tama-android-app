package com.tama.mealwhisperer.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.tama.mealwhisperer.entity.Ingredient;
import com.tama.mealwhisperer.entity.Recipe;
import com.tama.mealwhisperer.entity.RecipeIngredients;

import java.util.List;


/**
 * Created by teap3 on 29.10.2017.
 */

@Dao
public interface RecipeIngredientsDao {

    @Query("SELECT * FROM recipeingredients WHERE recipe_id = :id")
    List<RecipeIngredients> findRecipeIngredientsById(Integer id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertRecipeIngredients(RecipeIngredients recipeIngredients);

    @Query("DELETE FROM recipeIngredients WHERE recipe_id = :id")
    void deleteRecipeIngredientsById(Integer id);
}