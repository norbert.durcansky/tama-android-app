package com.tama.mealwhisperer.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.tama.mealwhisperer.entity.Ingredient;

import java.util.List;

/**
 * Created by norbert.durcansky on 10/9/2017.
 */

@Dao
public interface IngredientDao {

    @Query("SELECT * FROM ingredient")
    List<Ingredient> getAll();

    @Query("Select ingredient.uid,ingredient.name from ingredient " +
            "INNER JOIN recipeIngredients on recipeIngredients.ingredient_id=ingredient.uid and " +
            "recipeIngredients.recipe_id=:recipeId")
    List<Ingredient> getIngredientsForRecipe(Integer recipeId);

    @Query("SELECT * FROM ingredient WHERE uid IN (:userIds)")
    List<Ingredient> loadAllByIds(Integer[] userIds);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Ingredient ingredient);
}
