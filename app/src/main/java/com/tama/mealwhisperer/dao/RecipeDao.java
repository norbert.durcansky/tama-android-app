package com.tama.mealwhisperer.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.tama.mealwhisperer.entity.Recipe;

import java.util.List;

/**
 * Created by norbert.durcansky on 10/9/2017.
 */

@Dao
public interface RecipeDao {

    @Query("SELECT * FROM recipe")
    List<Recipe> getAllRecipes();

    @Query("SELECT * FROM recipe WHERE favorite=1")
    List<Recipe> getFavoriteRecipes();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertRecipe(Recipe recipe);

    @Update
    public void updateRecipe(Recipe recipe);

    @Delete
    void deleteRecipe(Recipe recipe);
}
