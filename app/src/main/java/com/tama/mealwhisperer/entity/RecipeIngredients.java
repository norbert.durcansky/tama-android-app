package com.tama.mealwhisperer.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by norbert.durcansky on 10/8/2017.
 */

@Entity(foreignKeys = {@ForeignKey(entity = Recipe.class,
        parentColumns = "uid",
        childColumns = "recipe_id"),@ForeignKey(entity = Ingredient.class,
        parentColumns = "uid",
        childColumns = "ingredient_id")})
public class RecipeIngredients {
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name = "recipe_id")
    public int recipeId;

    @ColumnInfo(name = "ingredient_id")
    public int ingredientId;

    @ColumnInfo(name = "ingredient_amount")
    public String ingredientAmount;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(int recipeId) {
        this.recipeId = recipeId;
    }

    public int getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(int ingredientId) {
        this.ingredientId = ingredientId;
    }

    public RecipeIngredients setIngredientIdO(int ingredientId) {
        this.ingredientId = ingredientId;
        return this;
    }

    public String getIngredientAmount() {
        return ingredientAmount;
    }

    public void setIngredientAmount(String ingredientAmount) {
        this.ingredientAmount = ingredientAmount;
    }

    public RecipeIngredients setIngredientAmountO(String ingredientAmount) {
        this.ingredientAmount = ingredientAmount;
        return this;
    }
}
