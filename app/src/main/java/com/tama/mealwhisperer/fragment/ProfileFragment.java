package com.tama.mealwhisperer.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tama.mealwhisperer.AppDatabase;
import com.tama.mealwhisperer.MainActivity;
import com.tama.mealwhisperer.MealWhisperer;

import com.tama.mealwhisperer.R;
import com.tama.mealwhisperer.adapter.RecipeAdapter;
import com.tama.mealwhisperer.entity.Recipe;
import com.tama.mealwhisperer.entity.User;
import com.tama.mealwhisperer.listener.BitmapStorageSaver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

/**
 * Created by teap3 on 10/4/2017.
 */

public class ProfileFragment extends Fragment {
    private final AppDatabase db = MealWhisperer.get().getAppDatabase(); //database handler
    private User user;                                                   //User profile info
    private EditText nameView;                                           //Views for name, age and
    private EditText ageView;                                            //favorite recipes
    private ListView listView;
    private CircleImageView profilePictureView;                          //View for profile picture
    private final int IMAGE_GALLERY_REQUEST = 20;
    private final int IMAGE_CAMERA = 21;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Save pointers to Views
        View inflatedView = inflater.inflate(R.layout.profile_fragment, container, false);
        nameView =  (EditText) inflatedView.findViewById(R.id.Name);
        ageView = (EditText) inflatedView.findViewById(R.id.Age);
        listView = (ListView) inflatedView.findViewById(R.id.FavRecipesList);
        profilePictureView = (CircleImageView) inflatedView.findViewById(R.id.profilePicture);


        this.user = this.db.userDao().getUser(); //Get user info

        //Set Content for user name View, user age View and List View
        setUserNameAndAge();
        setUserProfilePicture();
        setListViewContent();

        //Set handlers for Views
        setImageHandler();
        setEditTextHandlers();
        setListViewHandlers();

        return inflatedView;
    }

    /**
     * Method that sets profile picture at profile page
     */
    private void setUserProfilePicture()
    {
        String filePath = user.getPicturePath(); //Get path of picture

        if(filePath.startsWith("android.resource")) //Default profile picture
        {
            profilePictureView.setImageURI(null);
            profilePictureView.setImageURI(Uri.parse(filePath));
        }
        else //if user changed his profile photo
        {
            BitmapStorageSaver saver = new BitmapStorageSaver();
            Bitmap profilePicture = saver.loadImageFromStorage(filePath);
            this.profilePictureView.setImageBitmap(profilePicture);
        }
    }

    /**
     * This method sets up default values for user name and age
     */
    private void setUserNameAndAge ()
    {
        nameView.setText(user.getName());
        ageView.setText(user.getAge());
    }

    /**
     * This method sets up content in ListView with favorite recipes
     */
    private void setListViewContent()
    {
        //Get List of recipes and convert it to ArrayList
        List<Recipe> recipes = db.recipeDao().getFavoriteRecipes();
        ArrayList<Recipe> myRecipes =  new ArrayList<>(Arrays.asList(recipes.toArray(new Recipe[recipes.size()])));

        RecipeAdapter adbRecipe = new RecipeAdapter (
                getActivity(),
                R.layout.fav_recipes_list_view,
                R.id.fav_recipe_name, myRecipes);

        listView.setAdapter(adbRecipe);
    }

    /**
     * This method sets up the handler for profile image click
     */
    private void setImageHandler()
    {
        profilePictureView.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                CharSequence options[] = new CharSequence[] {"Take a photo",
                                                             "Choose from Gallery"};

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Select image source:");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) //Chosen from camera
                        {
                            Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(takePicture, IMAGE_CAMERA);
                        }
                        else //Chosen from gallery
                        {
                            Intent photoIntent = new Intent(Intent.ACTION_PICK); //Create Intent for action
                            File pictureDirectory = Environment.getExternalStoragePublicDirectory(
                                    Environment.DIRECTORY_PICTURES); //Get pictureDirectory

                            String pictureDirectoryPath = pictureDirectory.getPath(); //Convert it to string
                            Uri parsedUri = Uri.parse(pictureDirectoryPath);
                            photoIntent.setDataAndType(parsedUri, "image/*"); //Set that we are looking for images
                            startActivityForResult(photoIntent, IMAGE_GALLERY_REQUEST); //Start activity
                        }
                    }
                });
                builder.show();
            }
        });
    }

    /**
     * Processing of image chosen by user from gallery
     * @param requestCode indicates action that was performed
     * @param resultCode indicates if action was successfully completed
     * @param data data from action
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Check for result code and request code
        if (resultCode != RESULT_OK)
            return;

        if (requestCode != IMAGE_GALLERY_REQUEST && requestCode != IMAGE_CAMERA)
            return;

        Context appContext = MainActivity.getContextOfApplication();
        InputStream inputStream;

        try {
            if (requestCode == IMAGE_GALLERY_REQUEST) //From gallery
            {
                Uri chosenImageUri = data.getData(); //Get URI of picture chosen by user
                inputStream = appContext.getContentResolver().openInputStream(chosenImageUri);
                //Convert stream to bitmap and set new profile picture
                Bitmap image = BitmapFactory.decodeStream(inputStream);
                profilePictureView.setImageBitmap(image);

                //Save picture to internal storage and update picture path in database to make it persistent
                BitmapStorageSaver saver = new BitmapStorageSaver();
                this.user.setPicturePath(saver.saveBitmap(image));
                db.userDao().updateUser(this.user);
            }
            else if (requestCode == IMAGE_CAMERA) //From camera
            {
                Bundle extras = data.getExtras(); //Get image from camera
                Bitmap image = (Bitmap) extras.get("data");
                profilePictureView.setImageBitmap(image);

                //Save picture to storage and update picture in database
                BitmapStorageSaver saver = new BitmapStorageSaver();
                this.user.setPicturePath(saver.saveBitmap(image));
                db.userDao().updateUser(this.user);

            }

        } catch (FileNotFoundException e)
        {
            Toast.makeText(appContext, "File not found.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * This method sets up handler on click for items in ListView with favorite recipes
     */
    private void setListViewHandlers()
    {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                Recipe recipe = (Recipe) adapter.getItemAtPosition(position);

                Bundle bundle = new Bundle();
                bundle.putSerializable("recipe", recipe);
                bundle.putString("fromProfilePage", "");


                //Go to recipe detail fragment
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

                RecipeDetail recipeDetail = new RecipeDetail();
                recipeDetail.setArguments(bundle);

                ft.replace(R.id.layout_fragment, recipeDetail).commit();
            }
        });
    }

    /**
     * This method sets up handler for changing values in name and age EditViews
     */
    private void setEditTextHandlers()
    {
        //Handler when user clicks enter on keyboard in name box
        nameView.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    editUserName();
                    handled = true;
                }
                return handled;
            }
        });

        //Handler when user clicks enter on keyboard in age box:
        ageView.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    editUserAge();
                    handled = true;
                }
                return handled;
            }
        });

        //Handler when user loose focus on name box:
        nameView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    editUserName();
            }
        });

        //Handler when user loose focus on age box:
        ageView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    editUserAge();
            }
        });
    }

    /**
     * This method edits user's name changed by user
     */
    private void editUserName ()
    {
        user.setName(nameView.getText().toString()); //Update user's properties

        //Close the keyboard:
       InputMethodManager imm = (InputMethodManager)
                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
       imm.hideSoftInputFromWindow(nameView.getWindowToken(), 0);

       new Thread(new Runnable() { //Update database
           @Override
           public void run() {
                db.userDao().updateUser(user);
            }
       }).start();
    }

    /**
     * This method edits user's age changed by user
     */
    private void editUserAge ()
    {
        final String defaultAgeValue = "age";
        String age = ageView.getText().toString();

        //Check if input is number
        try {
            Integer.parseInt(age);
        } catch (Exception e)
        {
            //If provided age is not number
            ageView.setText(defaultAgeValue);
            ageView.setSelection(defaultAgeValue.length());
            user.setAge(defaultAgeValue);
            new Thread(new Runnable() { //Update database
                @Override
                public void run() {
                    db.userDao().updateUser(user);
                }
            }).start();
            return;
        }

        user.setAge(age); //Update user's properties

        //Close the keyboard:
        InputMethodManager imm = (InputMethodManager)
                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(ageView.getWindowToken(), 0);

        new Thread(new Runnable() { //Update database
            @Override
            public void run() {
                db.userDao().updateUser(user);
            }
        }).start();
    }
}
