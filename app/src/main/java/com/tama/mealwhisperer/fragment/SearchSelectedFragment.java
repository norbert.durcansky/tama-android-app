package com.tama.mealwhisperer.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tama.mealwhisperer.R;
import com.tama.mealwhisperer.adapter.IngredientSelectedAdapter;
import com.tama.mealwhisperer.entity.Ingredient;
import com.tama.mealwhisperer.listener.IngredientListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by norbert.durcansky on 10/4/2017.
 */


public class SearchSelectedFragment extends Fragment implements IngredientListener {
    RecyclerView recyclerSelectedView;
    IngredientListener ingredientListener;
    IngredientSelectedAdapter ingredientSelectedAdapter;

    public List<Ingredient> selectedIngredients = new ArrayList<>();

    public List<Ingredient> getSelectedIngredients() {
        return selectedIngredients;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar, menu);
    }


    public interface IngredientListener {
        void onRemoveIngredient(Ingredient ingredient);
        void onShowAddFragment();
        void onShowSearchFragment();
    }

    @Override
    public void onAddIngredientAdapter(Ingredient ingredient) {

    }

    @Override
    public void onRemoveAddIngredientAdapter(Ingredient ingredient){

    }

    @Override
    public void onRemoveIngredientAdapter(Ingredient ingredient) {
        ingredientListener.onRemoveIngredient(ingredient);
        selectedIngredients.remove(ingredient);
        ingredientSelectedAdapter.notifyDataSetChanged();

        if(selectedIngredients.size()==0){
            TextView  textView = (TextView) getView().findViewById(R.id.ingredients_header);
            textView.setVisibility(View.GONE);

            textView = (TextView) getView().findViewById(R.id.ingredients_empty);
            textView.setVisibility(View.VISIBLE);

            textView = (TextView) getView().findViewById(R.id.ingredients_empty_quote);
            textView.setVisibility(View.VISIBLE);

            ImageView image  = (ImageView) getView().findViewById(R.id.ingredients_empty_pic);
            image.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onAttach(Context context) {
        ingredientListener = (SearchSelectedFragment.IngredientListener) context;
        super.onAttach(context);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.menu_add) {
            ingredientListener.onShowAddFragment();
        }else if(id == R.id.menu_search){
            ingredientListener.onShowSearchFragment();
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.search_selected_fragment, container, false);
        setHasOptionsMenu(true);

        recyclerSelectedView = (RecyclerView) rootView.findViewById(R.id.ingredients_selected);
        ingredientSelectedAdapter = new IngredientSelectedAdapter(selectedIngredients, this.getContext(),this);
        recyclerSelectedView.setAdapter(ingredientSelectedAdapter);

        if(selectedIngredients.size()!=0){
            TextView  textView = (TextView) rootView.findViewById(R.id.ingredients_header);
            textView.setVisibility(View.VISIBLE);

            textView = (TextView) rootView.findViewById(R.id.ingredients_empty);
            textView.setVisibility(View.GONE);

            textView = (TextView) rootView.findViewById(R.id.ingredients_empty_quote);
            textView.setVisibility(View.GONE);

            ImageView image  = (ImageView) rootView.findViewById(R.id.ingredients_empty_pic);
            image.setVisibility(View.GONE);
        }


        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);


        return rootView;
    }

}