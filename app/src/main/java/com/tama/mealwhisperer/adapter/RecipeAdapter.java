package com.tama.mealwhisperer.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tama.mealwhisperer.AppDatabase;
import com.tama.mealwhisperer.MainActivity;
import com.tama.mealwhisperer.MealWhisperer;
import com.tama.mealwhisperer.R;
import com.tama.mealwhisperer.entity.Recipe;

import java.util.ArrayList;

/**
 * Created by teap3 on 24.10.2017.
 */

/**
 * Adapter for listView of favorite recipes
 */
public class RecipeAdapter extends ArrayAdapter<Recipe> {
    private ArrayList<Recipe> recipes;
    private static LayoutInflater inflater = null;
    private AppDatabase db = MealWhisperer.get().getAppDatabase();

    public RecipeAdapter (Activity activity, int viewResourceId, int recipeNameResourceId,
                          ArrayList<Recipe> _recipes) {
        super(activity, viewResourceId, recipeNameResourceId, _recipes);
        try
        {
            this.recipes = _recipes;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        catch (Exception e) {

        }
    }

    public int getCount() {
        return recipes.size();
    }

    @Override
    public Recipe getItem(int position){
        return recipes.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    private static class ViewHolder {
        private TextView recipeName;
        private ImageView removeFavoritesImage;
        private ImageView removeFromDatabaseImage;
    }

    @NonNull
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        View vi = convertView;
        final ViewHolder holder;
        try {
            if (convertView == null) {
                vi = inflater.inflate(R.layout.fav_recipes_list_view, null);
                holder = new ViewHolder();

                holder.recipeName = (TextView) vi.findViewById(R.id.fav_recipe_name);
                holder.removeFavoritesImage = (ImageView) vi.findViewById(R.id.favorite_icon);
                holder.removeFromDatabaseImage = (ImageView) vi.findViewById(R.id.delete_icon);

                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }

            holder.recipeName.setText(recipes.get(position).getName());
            holder.removeFavoritesImage.setImageResource(R.drawable.star);

            //Set Image only for recipes that were defined by user
            if (recipes.get(position).getUserDefined())
                holder.removeFromDatabaseImage.setImageResource(R.drawable.ic_delete_forever_black_24dp);
            else
                holder.removeFromDatabaseImage.setImageResource(android.R.color.transparent);
            //Listener for click on favorites button
            setOnClickListenerToFavoriteButton(holder.removeFavoritesImage, position);

            //Set callback only for user-defined recipes
            if (recipes.get(position).getUserDefined())
                setOnClickListenerToRemoveButton(holder.removeFromDatabaseImage, position);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return vi;
    }

    /**
     * Sets dialog box and listener when user clicks on favorite button
     * @param removeFavoritesImage pointer to ImageView
     * @param position position of button clicked
     */
    private void setOnClickListenerToFavoriteButton(ImageView removeFavoritesImage, final int position)
    {
            removeFavoritesImage.setOnClickListener(new View.OnClickListener() {
            Recipe recipe = recipes.get(position); //Get recipe that was clicked on
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                        .setNegativeButton("No", null)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            //Listener for clicking on Yes button
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String removeMessage = "Recipe: " + recipe.getName() +
                                        " was successfully removed" + " from favorites.";
                                //Show message:
                                Toast.makeText(MainActivity.getContextOfApplication(), removeMessage,
                                        Toast.LENGTH_SHORT).show();

                                //Update recipe in database
                                recipe.setFavorite(false);
                                db.recipeDao().updateRecipe(recipe);

                                //Remove recipe from collection:
                                recipes.remove(position);
                                notifyDataSetChanged(); //Refresh listView
                            }}).create();

                alertDialog.setTitle("Warning");
                alertDialog.setMessage("Do you really want to remove " + recipe.getName() +
                        " from your FAVORITES?");
                alertDialog.show();
            }
        });
    }

    private void setOnClickListenerToRemoveButton (ImageView removeFromDatabaseImage, final int position)
    {
        removeFromDatabaseImage.setOnClickListener(new View.OnClickListener() {
            Recipe recipe = recipes.get(position); //Get recipe that was clicked on
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                        .setNegativeButton("No", null)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            //Listener for clicking on Yes button
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String removeMessage = "Recipe: " + recipe.getName() +
                                        " was successfully deleted" + " from database.";
                                //Show message:
                                Toast.makeText(MainActivity.getContextOfApplication(), removeMessage,
                                        Toast.LENGTH_SHORT).show();

                                db.recipeIngredientsDao().deleteRecipeIngredientsById(recipe.getUid());
                                db.recipeDao().deleteRecipe(recipe);
                                //TODO: Smazat obrazek receptu z internal storage

                                //Remove recipe from collection:
                                recipes.remove(position);
                                notifyDataSetChanged(); //Refresh listView
                            }}).create();

                alertDialog.setTitle("Warning");
                alertDialog.setMessage("Do you really want to completely delete " + recipe.getName()
                        + " from your DATABASE?");
                alertDialog.show();
            }
        });
    }
}