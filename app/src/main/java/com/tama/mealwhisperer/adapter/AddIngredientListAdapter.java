package com.tama.mealwhisperer.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tama.mealwhisperer.MealWhisperer;
import com.tama.mealwhisperer.R;
import com.tama.mealwhisperer.entity.Ingredient;
import com.tama.mealwhisperer.listener.IngredientListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hpros on 28. 10. 2017.
 */

public class AddIngredientListAdapter extends RecyclerView.Adapter<AddIngredientListAdapter.IngredientViewHolder> {
    private static List<Ingredient> list=new ArrayList<>();

    private Context mContext;

    private IngredientListener mAdapterCallback;

    public AddIngredientListAdapter(List<Ingredient> list, Context mContext,IngredientListener callback) {
        this.list=list;
        this.mContext = mContext;
        this.mAdapterCallback = callback;
    }

    @Override
    public AddIngredientListAdapter.IngredientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_ingredient_view, parent, false);
        return new AddIngredientListAdapter.IngredientViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AddIngredientListAdapter.IngredientViewHolder holder, int position) {
        holder.bind(list.get(position));
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAdapterCallback.onAddIngredientAdapter(list.get((int)view.getTag()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class IngredientViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private Button button;


        public IngredientViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.add_ingredient_name);
            button = itemView.findViewById(R.id.add_ingredient_add);
        }

        public void bind(Ingredient ingredient) {
            name.setText(ingredient.getName());
            button.setTag(getAdapterPosition());

        }
    }

    public void filter(final String text, final List<Ingredient> selected) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                list.clear();
                List<Ingredient> items = MealWhisperer.get().getAppDatabase().ingredientDao().getAll();

                for(Ingredient selectedIngredient : selected){
                    items.contains(selectedIngredient);
                    items.remove(selectedIngredient);
                }
                if (!TextUtils.isEmpty(text)){
                    for (Ingredient item : items) {
                        if (item.getName().toLowerCase().contains(text.toLowerCase())) {
                            list.add(item);
                        }
                    }
                }else{
                    list.addAll(items);
                }

                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                    }
                });

            }
        }).start();

    }
}
