package com.tama.mealwhisperer.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tama.mealwhisperer.R;
import com.tama.mealwhisperer.entity.Ingredient;
import com.tama.mealwhisperer.listener.IngredientListener;

import java.util.List;

/**
 * Created by hpros on 28. 10. 2017.
 */

public class AddIngredientAdapter extends RecyclerView.Adapter<AddIngredientAdapter.IngredientViewHolder> {
    private List<Ingredient> list;

    private IngredientListener mAdapterCallback;


    public AddIngredientAdapter(List<Ingredient> list, IngredientListener callback) {
        this.list = list;
        /* callback to send data to MainActivity */
        this.mAdapterCallback = callback;
    }

    @Override
    public AddIngredientAdapter.IngredientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_selected_view, parent, false);
        return new AddIngredientAdapter.IngredientViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AddIngredientAdapter.IngredientViewHolder holder, int position) {
        holder.bind(list.get(position));
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAdapterCallback.onRemoveAddIngredientAdapter(list.get((int) view.getTag()));
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class IngredientViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private Button button;
        private EditText amount;

        public IngredientViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.add_selected_name);
            button = itemView.findViewById(R.id.add_ingredient_remove);
            amount = itemView.findViewById(R.id.amount);
        }

        public void bind(Ingredient ingredient) {
            name.setText(ingredient.getName());
            button.setTag(getAdapterPosition());

        }
    }
}


