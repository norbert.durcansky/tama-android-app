package com.tama.mealwhisperer.adapter;

/**
 * Created by norbert.durcansky on 10/29/2017.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tama.mealwhisperer.R;
import com.tama.mealwhisperer.entity.Recipe;
import com.tama.mealwhisperer.listener.BitmapStorageSaver;

import java.util.List;

/**
 * Created by norbert.durcansky on 10/9/2017.
 */
public class SwipeAdapter extends ArrayAdapter<Recipe> {
    public SwipeAdapter(Context context, List<Recipe> recipes) {
        super(context, 0, recipes);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Recipe recipe = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.recipes_swipe_item, parent, false);
        }

        ImageView image = (ImageView) convertView.findViewById(R.id.recipe_detail_picture) ;
        TextView prepTimeView = (TextView)  convertView.findViewById(R.id.recipe_prep_time);
        TextView portionCountView =(TextView) convertView.findViewById(R.id.recipe_portion_count);

        //Set preparation time
        prepTimeView.setText(recipe.getPrepTime());
        prepTimeView.getBackground().setAlpha(90);

        //Set portion count
        Integer portionCount = recipe.getPortionCount();
        if (portionCount > 1)
            portionCountView.setText(portionCount.toString() + " portions");
        else
            portionCountView.setText(portionCount.toString() + " portion");

        if(recipe.getPicturePath().startsWith("/data")){
            BitmapStorageSaver saver = new BitmapStorageSaver();
            Bitmap profilePicture = saver.loadRecipeImage(recipe.getPicturePath());
            image.setImageBitmap(profilePicture);
        } else{
            // get pictures of default recipes
            image.setImageResource(convertView.getResources().getIdentifier
                    (recipe.getPicturePath(), "drawable", "com.tama.mealwhisperer"));
        }

        return convertView;
    }
}